package com.javastart.hibernatetest.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private Integer age;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL) // EARLIER - EAGER
    private List<Bill>  bills;

    public List<Bill> getBills() {
        return bills;
    }
    public void setBills(List<Bill> bills) {
        this.bills = bills;
    }
    public Account(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
    public Account() {

    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    @Override
    public String toString() {
        return "Account{" + "id=" + id + ", name='" + name + '\'' +
                ", age=" + age + '}';
    }
}
